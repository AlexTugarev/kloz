from django.urls import path
from .views.board import ListBoardGenericAPIView, CreateBoardGenericAPIView, BoardGenericAPIView
from .views.company import ListCompanyGenericAPIView, CreateCompanyGenericAPIView, CompanyGenericAPIView
from .views.response import ListResponseGenericAPIView, CreateResponseGenericAPIView, ResponseGenericAPIView
from .views.subject import ListSubjectGenericAPIView, CreateSubjectGenericAPIView, SubjectGenericAPIView
from .views.vote import ListVoteGenericAPIView, CreateVoteGenericAPIView, VoteGenericAPIView


urlpatterns = [
  path('boards/', ListBoardGenericAPIView.as_view()),
  path('board/', CreateBoardGenericAPIView.as_view()),
  path('board/<int:id>/', BoardGenericAPIView.as_view()),

  path('companies/', ListCompanyGenericAPIView.as_view()),
  path('company/', CreateCompanyGenericAPIView.as_view()),
  path('company/<int:id>/', CompanyGenericAPIView.as_view()),

  path('responses/', ListResponseGenericAPIView.as_view()),
  path('response/', CreateResponseGenericAPIView.as_view()),
  path('response/<int:id>/', ResponseGenericAPIView.as_view()),

  path('subjects/', ListSubjectGenericAPIView.as_view()),
  path('subject/', CreateSubjectGenericAPIView.as_view()),
  path('subject/<int:id>/', SubjectGenericAPIView.as_view()),

  path('votes/', ListVoteGenericAPIView.as_view()),
  path('vote/', CreateVoteGenericAPIView.as_view()),
  path('vote/<int:id>/', VoteGenericAPIView.as_view()),
]
