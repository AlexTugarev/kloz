import operator
from django.views.decorators.csrf import csrf_exempt
from rest_framework.views import APIView
from rest_framework import generics, mixins


from ..models import Subject
from ..serializers.subject import SubjectSerializer


##################################################################################
#
#  RESPONSE VIEWS
#
##################################################################################

class ListSubjectGenericAPIView(generics.GenericAPIView, mixins.ListModelMixin):
  serializer_class = SubjectSerializer
  queryset = Subject.objects.all()

  @csrf_exempt
  def get(self, request):
    return self.list(request)

class CreateSubjectGenericAPIView(
  generics.GenericAPIView,
  mixins.CreateModelMixin):

  serializer_class = SubjectSerializer
  queryset = Subject.objects.all()

  @csrf_exempt
  def post(self, request):
    return self.create(request)


class SubjectGenericAPIView(
  generics.GenericAPIView,
  mixins.RetrieveModelMixin,
  mixins.UpdateModelMixin,
  mixins.DestroyModelMixin):

  serializer_class = SubjectSerializer
  queryset = Subject.objects.all()
  lookup_field = 'id'

  @csrf_exempt
  def get(self, request, id):
    return self.retrieve(request, id)

  @csrf_exempt
  def put(self, request, id=None):
    return self.update(request, id)

  @csrf_exempt
  def delete(self, request, id=None):
    return self.destroy(request, id)
