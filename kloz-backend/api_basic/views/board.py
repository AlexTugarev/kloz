import operator
from django.views.decorators.csrf import csrf_exempt
from rest_framework.views import APIView
from rest_framework import generics, mixins


from ..models import Board
from ..serializers.board import BoardSerializer


##################################################################################
#
#  BOARD VIEWS
#
##################################################################################

class ListBoardGenericAPIView(generics.GenericAPIView, mixins.ListModelMixin):
  serializer_class = BoardSerializer
  queryset = Board.objects.all()

  @csrf_exempt
  def get(self, request):
    return self.list(request)

class CreateBoardGenericAPIView(
  generics.GenericAPIView,
  mixins.CreateModelMixin):

  serializer_class = BoardSerializer
  queryset = Board.objects.all()

  @csrf_exempt
  def post(self, request):
    return self.create(request)


class BoardGenericAPIView(
  generics.GenericAPIView,
  mixins.RetrieveModelMixin,
  mixins.UpdateModelMixin,
  mixins.DestroyModelMixin):

  serializer_class = BoardSerializer
  queryset = Board.objects.all()
  lookup_field = 'id'

  @csrf_exempt
  def get(self, request, id):
    return self.retrieve(request, id)

  @csrf_exempt
  def put(self, request, id=None):
    return self.update(request, id)

  @csrf_exempt
  def delete(self, request, id=None):
    return self.destroy(request, id)
