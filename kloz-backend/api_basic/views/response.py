import operator
from django.views.decorators.csrf import csrf_exempt
from rest_framework.views import APIView
from rest_framework import generics, mixins


from ..models import Response
from ..serializers.response import ResponseSerializer


##################################################################################
#
#  RESPONSE VIEWS
#
##################################################################################

class ListResponseGenericAPIView(generics.GenericAPIView, mixins.ListModelMixin):
  serializer_class = ResponseSerializer
  queryset = Response.objects.all()

  @csrf_exempt
  def get(self, request):
    return self.list(request)

class CreateResponseGenericAPIView(
  generics.GenericAPIView,
  mixins.CreateModelMixin):

  serializer_class = ResponseSerializer
  queryset = Response.objects.all()

  @csrf_exempt
  def post(self, request):
    return self.create(request)


class ResponseGenericAPIView(
  generics.GenericAPIView,
  mixins.RetrieveModelMixin,
  mixins.UpdateModelMixin,
  mixins.DestroyModelMixin):

  serializer_class = ResponseSerializer
  queryset = Response.objects.all()
  lookup_field = 'id'

  @csrf_exempt
  def get(self, request, id):
    return self.retrieve(request, id)

  @csrf_exempt
  def put(self, request, id=None):
    return self.update(request, id)

  @csrf_exempt
  def delete(self, request, id=None):
    return self.destroy(request, id)
