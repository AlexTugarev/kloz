import operator
from django.views.decorators.csrf import csrf_exempt
from rest_framework.views import APIView
from rest_framework import generics, mixins


from ..models import Company
from ..serializers.company import CompanySerializer


##################################################################################
#
#  COMPANY VIEWS
#
##################################################################################

class ListCompanyGenericAPIView(generics.GenericAPIView, mixins.ListModelMixin):
  serializer_class = CompanySerializer
  queryset = Company.objects.all()

  @csrf_exempt
  def get(self, request):
    return self.list(request)

class CreateCompanyGenericAPIView(
  generics.GenericAPIView,
  mixins.CreateModelMixin):

  serializer_class = CompanySerializer
  queryset = Company.objects.all()

  @csrf_exempt
  def post(self, request):
    return self.create(request)


class CompanyGenericAPIView(
  generics.GenericAPIView,
  mixins.RetrieveModelMixin,
  mixins.UpdateModelMixin,
  mixins.DestroyModelMixin):

  serializer_class = CompanySerializer
  queryset = Company.objects.all()
  lookup_field = 'id'

  @csrf_exempt
  def get(self, request, id):
    return self.retrieve(request, id)

  @csrf_exempt
  def put(self, request, id=None):
    return self.update(request, id)

  @csrf_exempt
  def delete(self, request, id=None):
    return self.destroy(request, id)
