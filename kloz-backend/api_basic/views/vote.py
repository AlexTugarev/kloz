import operator
from django.views.decorators.csrf import csrf_exempt
from rest_framework.views import APIView
from rest_framework import generics, mixins


from ..models import Vote
from ..serializers.vote import VoteSerializer


##################################################################################
#
#  RESPONSE VIEWS
#
##################################################################################

class ListVoteGenericAPIView(generics.GenericAPIView, mixins.ListModelMixin):
  serializer_class = VoteSerializer
  queryset = Vote.objects.all()

  @csrf_exempt
  def get(self, request):
    return self.list(request)

class CreateVoteGenericAPIView(
  generics.GenericAPIView,
  mixins.CreateModelMixin):

  serializer_class = VoteSerializer
  queryset = Vote.objects.all()

  @csrf_exempt
  def post(self, request):
    return self.create(request)


class VoteGenericAPIView(
  generics.GenericAPIView,
  mixins.RetrieveModelMixin,
  mixins.UpdateModelMixin,
  mixins.DestroyModelMixin):

  serializer_class = VoteSerializer
  queryset = Vote.objects.all()
  lookup_field = 'id'

  @csrf_exempt
  def get(self, request, id):
    return self.retrieve(request, id)

  @csrf_exempt
  def put(self, request, id=None):
    return self.update(request, id)

  @csrf_exempt
  def delete(self, request, id=None):
    return self.destroy(request, id)
