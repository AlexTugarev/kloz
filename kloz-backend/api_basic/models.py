from django.db import models
from django.conf import settings
from django.contrib.auth.models import AbstractUser


class User(AbstractUser):
  pass

class Company(models.Model):
  name               = models.CharField(max_length=100, primary_key=True, unique=True)
  referent           = models.ForeignKey(User, on_delete=models.CASCADE)

  created_at         = models.DateTimeField(auto_now_add=True)

  def __str__(self):
    return self.name

class Board(models.Model):
  company            = models.ForeignKey(Company, on_delete=models.CASCADE)
  name               = models.CharField(max_length=100)

  created_at         = models.DateTimeField(auto_now_add=True)

  def __str__(self):
    print('iddddd')
    return self.name

class Subject(models.Model):
  board              = models.ForeignKey(Board, on_delete=models.CASCADE)
  creator            = models.ForeignKey(User, on_delete=models.CASCADE)
  title              = models.CharField(max_length=255)
  description        = models.CharField(max_length=255)
  state              = models.CharField(max_length=100)

  created_at         = models.DateTimeField(auto_now_add=True)

  def __str__(self):
    return self.title

class Response(models.Model):
  author             = models.ForeignKey(User, on_delete=models.CASCADE)
  subject            = models.ForeignKey(Subject, on_delete=models.CASCADE)
  description        = models.CharField(max_length=255)

  created_at         = models.DateTimeField(auto_now_add=True)

  def __str__(self):
    return 'Response: {}'.format(self.description)

class Vote(models.Model):
  author             = models.ForeignKey(User, on_delete=models.CASCADE)
  subject            = models.ForeignKey(Subject, on_delete=models.CASCADE)
  value              = models.IntegerField()

  created_at         = models.DateTimeField(auto_now_add=True)

  def __str__(self):
    return 'Vote: {}'.format(self.value)
