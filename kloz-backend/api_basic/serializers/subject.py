from rest_framework import serializers
from ..models import Subject


class SubjectSerializer(serializers.ModelSerializer):
  id = serializers.ReadOnlyField()
  created_at = serializers.ReadOnlyField()

  class Meta:
    model = Subject
    fields = '__all__'
