from rest_framework import serializers
from ..models import Company


class CompanySerializer(serializers.ModelSerializer):
  name = serializers.ReadOnlyField()
  created_at = serializers.ReadOnlyField()

  class Meta:
    model = Company
    fields = '__all__'
