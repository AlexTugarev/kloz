from rest_framework import serializers
from ..models import Vote


class VoteSerializer(serializers.ModelSerializer):
  id = serializers.ReadOnlyField()
  created_at = serializers.ReadOnlyField()

  class Meta:
    model = Vote
    fields = '__all__'
