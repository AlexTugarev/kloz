import Vue from 'vue';
import VueRouter from 'vue-router';
import Board from '../views/Board.vue';
import Signin from '../views/Signin.vue';
import Login from '../views/Login.vue';
import Response from '../views/Response.vue';
import About from '../views/About.vue';

Vue.use(VueRouter);

const routes = [
  {
    path: '/',
    name: 'Signin',
    component: Signin,
  },
  {
    path: '/login',
    name: 'Login',
    component: Login,
  },
  {
    path: '/board',
    name: 'Board',
    component: Board,
  },
  {
    path: '/response',
    name: 'Response',
    component: Response,
  },
  {
    path: '/about',
    name: 'About',
    component: About,
  },
];

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes,
});

export default router;
