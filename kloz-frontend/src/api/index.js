import axios from 'axios';
import ENV from '@/environments';


const HTTP = axios.create({
  baseURL: `${ENV.API_URL}`,
  withCredentials: false,
  headers: {
    Accept: 'application/json',
    'Content-Type': 'application/json',
  },
});

export default {
  // Boards services
  getBoards() {
    return HTTP.get('/boards/');
  },
  createBoard(board) {
    return HTTP.post('/board/̀', board);
  },
  getBoard(boardId) {
    return HTTP.get(`/board/̀${boardId}`);
  },
  updateBoard(boardId) {
    return HTTP.put(`/board/̀${boardId}`);
  },
  deleteBoard(boardId) {
    return HTTP.delete(`/board/̀${boardId}`);
  },
  // Company services
  getCompanies() {
    return HTTP.get('/companies/');
  },
  createCompany(company) {
    return HTTP.post('/company/̀', company);
  },
  getCompany(companyName) {
    return HTTP.get(`/company/̀${companyName}`);
  },
  updateCompany(companyName) {
    return HTTP.put(`/company/̀${companyName}`);
  },
  deleteCompany(companyName) {
    return HTTP.delete(`/company/̀${companyName}`);
  },
  // Response services
  getResponses() {
    return HTTP.get('/responses/');
  },
  createResponse(response) {
    return HTTP.post('/response/̀', response);
  },
  getResponse(responseId) {
    return HTTP.get(`/response/̀${responseId}`);
  },
  updateResponse(responseId) {
    return HTTP.put(`/response/̀${responseId}`);
  },
  deleteResponse(responseId) {
    return HTTP.delete(`/response/̀${responseId}`);
  },
  // Subject services
  getSubjects() {
    return HTTP.get('/subjects/');
  },
  createSubject(subject) {
    return HTTP.post('/subject/̀', subject);
  },
  getSubject(subjectId) {
    return HTTP.get(`/subject/̀${subjectId}`);
  },
  updateSubject(subjectId) {
    return HTTP.put(`/subject/̀${subjectId}`);
  },
  deleteSubject(subjectId) {
    return HTTP.delete(`/subject/̀${subjectId}`);
  },
  // Vote services
  getVotes() {
    return HTTP.get('/votes/');
  },
  createVote(vote) {
    return HTTP.post('/vote/̀', vote);
  },
  getVote(voteId) {
    return HTTP.get(`/vote/̀${voteId}`);
  },
  updateVote(voteId) {
    return HTTP.put(`/vote/̀${voteId}`);
  },
  deleteVote(voteId) {
    return HTTP.delete(`/vote/̀${voteId}`);
  },
};
